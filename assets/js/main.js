(function () {
  var app = angular.module('counterApp', ['ngTouch']);

  app.service('StorageService', [function () {
    var storage = {};

    storage.save = function (val) {
      localStorage.setItem('count', val);
    };

    storage.get = function () {
      var val = localStorage.getItem('count');

      return (val) ? parseFloat(val) : 0;
    };

    return storage;
  }]);

  app.controller('CounterController', ['$scope', 'StorageService', function ($scope, storage) {
    //Format number to array
    $scope.formatNumber = function () {
      var num    = $scope.count,
          format = num.toString().split(''),
          dif    = 4 - format.length;

      for (var i = 0; i < dif; i++) {
        format.unshift(0);
      };

      $scope.formated = format;
    };

    //Scope number
    $scope.count = storage.get();


    //Watch number change
    $scope.$watch('count', function () {
      storage.save($scope.count);
      $scope.formatNumber();
    });

    // +1
    $scope.plus = function () {
      var count = $scope.count + 1;

      $scope.count = count;
      window.focus();
    };

    // -1
    $scope.less = function () {
      var count = $scope.count -1;

      if (count < 0) {
        $scope.count = 0;

      } else {
        $scope.count = count;
      }
      window.focus();
    };

    $scope.reset = function () {
      var reset = window.confirm("Quer zerar o contador?");

      if (reset) {
        $scope.count = 0;
      }
    };
  }]);

  //Prevent default behaviour in iPad/iPhone
  document.addEventListener('touchmove', function (event) {
    event.preventDefault();
  });
})();
