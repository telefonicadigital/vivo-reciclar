#VIVO Reciclar
Aplicativo feito em [HarpJS](http://harpjs.com/) criado para a ação da área de sustentabilidade na Campus Party 2015.

Para testar - [http://telefonicadigital.github.io/vivo-reciclar/](http://telefonicadigital.github.io/vivo-reciclar/).
